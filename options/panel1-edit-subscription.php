<?php
if ( ! function_exists( 'is_admin' ) || ! is_admin() ) {
	header( 'Location: /' );
	exit;
}
?>
<div class="postbox">
	<h3><?php _e( 'Update Subscription', 'its-comments-subscribe' ) ?></h3>

	<form action="options-general.php?page=its-comments-subscribe/options/index.php&subscribepanel=1" method="post" id="update_address_form"
		  onsubmit="if (this.sre.value != '<?php _e( 'optional', 'its-comments-subscribe' ) ?>') return confirm('<?php _e( 'Please remember: this operation cannot be undone. Are you sure you want to proceed?', 'its-comments-subscribe' ) ?>')">
		<fieldset style="border:0">
			<p><?php _e( 'Post:', 'its-comments-subscribe' );
echo ' <strong>' . get_the_title( intval( $_GET['srp'] ) ) . " ({$_GET['srp']})"; ?></strong></p>

			<p class="liquid"><label for='oldsre'><?php _e( 'From', 'its-comments-subscribe' ) ?></label>
				<input readonly='readonly' type='text' size='30' name='oldsre' id='oldsre' value='<?php echo $_GET['sre'] ?>' />
			</p>

			<p class="liquid"><label for='sre'><?php _e( 'To', 'its-comments-subscribe' ) ?></label>
				<input type='text' size='30' name='sre' id='sre' value='<?php _e( 'optional', 'its-comments-subscribe' ) ?>' style="color:#ccc"
					   onfocus='if (this.value == "<?php _e( 'optional', 'its-comments-subscribe' ) ?>") this.value="";this.style.color="#000"'
					   onblur='if (this.value == ""){this.value="<?php _e( 'optional', 'its-comments-subscribe' ) ?>";this.style.color="#ccc"}' />
			</p>

			<p class="liquid"><label for='srs'><?php _e( 'Status', 'its-comments-subscribe' ) ?></label>
				<select name="srs" id="srs">
					<option value=''><?php _e( 'Keep unchanged', 'its-comments-subscribe' ) ?></option>
					<option value='Y'><?php _e( 'Receive Notification For All New Comments', 'its-comments-subscribe' ) ?></option>
					<option value='R'><?php _e( 'Receive Comment Reply Notification Only', 'its-comments-subscribe' ) ?></option>
					<option value='C'><?php _e( 'Suspended', 'its-comments-subscribe' ) ?></option>
				</select>
				<input type='submit' class='subscribe-form-button' value='<?php _e( 'Update', 'its-comments-subscribe' ) ?>' />
			</p>
			<input type='hidden' name='sra' value='edit' />
			<input type='hidden' name='srp' value='<?php echo intval( $_GET['srp'] ) ?>' />
		</fieldset>
	</form>
</div>
