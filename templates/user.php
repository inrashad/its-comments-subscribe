<?php
// Avoid direct access to this piece of code
if (!function_exists('add_action')) {
    header('Location: /');
    exit;
}

global $wp_subscribe_reloaded;

ob_start();
if (!empty($_POST['post_list'])) {
    $post_list = array();
    foreach ($_POST['post_list'] as $a_post_id) {
        if (!in_array($a_post_id, $post_list)) {
            $post_list[] = intval($a_post_id);
        }
    }

    $action = !empty($_POST['sra']) ? $_POST['sra'] : (!empty($_GET['sra']) ? $_GET['sra'] : '' );
    switch ($action) {
        case 'delete':
            $rows_affected = $wp_subscribe_reloaded->delete_subscriptions($post_list, $email);
            echo '<p class="updated">' . __('Subscriptions deleted:', 'its-comments-subscribe') . " $rows_affected</p>";
            break;
        case 'suspend':
            $rows_affected = $wp_subscribe_reloaded->update_subscription_status($post_list, $email, 'C');
            echo '<p class="updated">' . __('Subscriptions suspended:', 'its-comments-subscribe') . " $rows_affected</p>";
            break;
        case 'activate':
            $rows_affected = $wp_subscribe_reloaded->update_subscription_status($post_list, $email, '-C');
            echo '<p class="updated">' . __('Subscriptions activated:', 'its-comments-subscribe') . " $rows_affected</p>";
            break;
        case 'force_y':
            $rows_affected = $wp_subscribe_reloaded->update_subscription_status($post_list, $email, 'Y');
            echo '<div class="updated"><p>' . __('Subscriptions updated:', 'its-comments-subscribe') . " $rows_affected</p></div>";
            break;
        case 'force_r':
            $rows_affected = $wp_subscribe_reloaded->update_subscription_status($post_list, $email, 'R');
            echo '<p class="updated">' . __('Subscriptions updated:', 'its-comments-subscribe') . " $rows_affected</p>";
            break;
        default:
            break;
    }
}
$message = html_entity_decode(stripslashes(get_option('subscribe_reloaded_user_text')), ENT_COMPAT, 'UTF-8');
if (function_exists('qtrans_useCurrentLanguageIfNotFoundUseDefaultLanguage')) {
    $message = qtrans_useCurrentLanguageIfNotFoundUseDefaultLanguage($message);
}
echo "<p>$message</p>";
?>

<form action="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']) ?>" method="post" id="post_list_form" name="post_list_form" onsubmit="if (this.sra[0].checked)
                    return confirm('<?php _e('Please remember: this operation cannot be undone. Are you sure you want to proceed?', 'its-comments-subscribe') ?>')">
    <fieldset style="border:0">
        <?php
        $subscriptions = $wp_subscribe_reloaded->get_subscriptions('email', 'equals', $email, 'dt', 'DESC');
        if (is_array($subscriptions) && !empty($subscriptions)) {
            echo '<p id="its-comments-subscribe-email-p">' . __('Email', 'its-comments-subscribe') . ': <strong>' . $email . '</strong></p>';
            echo '<p id="its-comments-subscribe-legend-p">' . __('Legend: Y = Receive Notification For All New Comments, R = Receive Comment Reply Notification Only, C = inactive', 'its-comments-subscribe') . '</p>';
            echo '<ul id="its-comments-subscribe-list">';
            foreach ($subscriptions as $a_subscription) {
                $permalink = get_permalink($a_subscription->post_id);
                $title = get_the_title($a_subscription->post_id);
                echo "<li><label for='post_{$a_subscription->post_id}'><input type='checkbox' name='post_list[]' value='{$a_subscription->post_id}' id='post_{$a_subscription->post_id}'/> <span class='subscribe-column-1'>$a_subscription->dt</span> <span class='subscribe-separator subscribe-separator-1'>&mdash;</span> <span class='subscribe-column-2'>{$a_subscription->status}</span> <span class='subscribe-separator subscribe-separator-2'>&mdash;</span> <a class='subscribe-column-3' href='$permalink'>$title</a></label></li>\n";
            }
            echo '</ul>';
            echo '<p id="its-comments-subscribe-select-all-p"><a class="its-comments-subscribe-small-button" href="#" onclick="t=document.forms[\'post_list_form\'].elements[\'post_list[]\'];c=t.length;if(!c){t.checked=true}else{for(var i=0;i<c;i++){t[i].checked=true}};return false;">' . __('Select all', 'its-comments-subscribe') . '</a> ';
            echo '<a class="its-comments-subscribe-small-button" href="#" onclick="t=document.forms[\'post_list_form\'].elements[\'post_list[]\'];c=t.length;if(!c){t.checked=!t.checked}else{for(var i=0;i<c;i++){t[i].checked=false}};return false;">' . __('Invert selection', 'its-comments-subscribe') . '</a></p>';
            echo '<p id="its-comments-subscribe-action-p">' . __('Action:', 'its-comments-subscribe') . '
			<input type="radio" name="sra" value="delete" id="action_type_delete" /> <label for="action_type_delete">' . __('Delete', 'its-comments-subscribe') . '</label> &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="sra" value="suspend" id="action_type_suspend" checked="checked" /> <label for="action_type_suspend">' . __('Suspend', 'its-comments-subscribe') . '</label> &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="sra" value="force_r" id="action_type_force_y" /> <label for="action_type_force_y">' . __('Receive Comment Reply Notification Only', 'its-comments-subscribe') . '</label> &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="sra" value="force_y" id="action_type_force_r" /> <label for="action_type_force_r">' . __('Receive Notification For All New Comments', 'its-comments-subscribe') . '</label></p>';
            echo '<p id="its-comments-subscribe-update-p"><input type="submit" class="subscribe-form-button" value="' . __('Update subscriptions', 'its-comments-subscribe') . '" /><input type="hidden" name="sre" value="' . urlencode($email) . '"/></p>';
        } else {
            echo '<p>' . __('No subscriptions match your search criteria.', 'its-comments-subscribe') . '</p>';
        }
        ?>
    </fieldset>
</form>
<?php
$output = ob_get_contents();
ob_end_clean();
return $output;
?>
